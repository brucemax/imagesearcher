# README #

Simple android app for searching images in google

### Use technologies and libs ###
* [Rx java](https://github.com/ReactiveX/RxAndroid)
* [Retrofit](http://square.github.io/retrofit/)
* [picasso](https://github.com/square/picasso)

### Adventures ###

* Support android 2.3 (API 9)   (It wouldn't be superfluous to have 5% of additional users :) )
* Support voice input

![screenshot.png](https://bitbucket.org/repo/bd49BE/images/482241458-screenshot.png)