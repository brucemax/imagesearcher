// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.brucemax.imagesearcher.widgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.brucemax.imagesearcher.R;

public class ClearEditText extends EditText {

    private Drawable mClearDrawable;

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            handleClear();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            handleClear();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private OnTouchListener mTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionevent)
        {
            if (getCompoundDrawables()[2] == null
                    || motionevent.getAction() != MotionEvent.ACTION_UP
                    || motionevent.getX() <= (float)(view.getWidth() - view.getPaddingRight() - mClearDrawable.getIntrinsicWidth())) {

                return false;
            }

            setText("");
            handleClear();
            return false;
        }
    };

    public ClearEditText(Context context) {
        super(context);
        init();
    }

    public ClearEditText(Context context, AttributeSet attributeset) {
        super(context, attributeset);
        init();
    }

    public ClearEditText(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        init();
    }

    private void handleClear() {
        if (TextUtils.isEmpty(getText())) {
            setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1], null, getCompoundDrawables()[3]);
        } else {
            setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1], mClearDrawable, getCompoundDrawables()[3]);
        }
    }

    @SuppressWarnings("deprecation")
    private void init() {
        mClearDrawable = getResources().getDrawable(R.drawable.ic_cancel);
        if (mClearDrawable != null) {
            mClearDrawable.setBounds(0, 0, mClearDrawable.getIntrinsicWidth(), mClearDrawable.getIntrinsicHeight());
        }
        handleClear();
        setOnTouchListener(mTouchListener);
        addTextChangedListener(mTextWatcher);
    }


}
