package com.brucemax.imagesearcher;

import android.app.Application;
import android.content.pm.ApplicationInfo;

public class ImagerSearcherApplication extends Application {
    public static boolean IS_DEBUG;

    @Override
    public void onCreate() {
        super.onCreate();
        IS_DEBUG = (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
    }
}
