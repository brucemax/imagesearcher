package com.brucemax.imagesearcher;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.brucemax.imagesearcher.adapters.ImageAdapter;
import com.brucemax.imagesearcher.models.MetaData;
import com.brucemax.imagesearcher.models.SearchResponse;
import com.brucemax.imagesearcher.networking.Api;
import com.brucemax.imagesearcher.networking.SearcherRestAdapter;
import com.brucemax.imagesearcher.rx.Events;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivityFragment extends Fragment {

    private static final int SPEECH_REQUEST_CODE = 0;

    private TextView.OnEditorActionListener mEditorActionListener;

    private ImageAdapter mAdapter;
    private Api.SearchApi mSearchApi;

    private TextView tvEmpty;
    private GridView gvImages;
    private EditText etSearchView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initUI(view);
        initRetrofit();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initScrollObservable();
        initTextObservables();
    }

    // Create an intent that can start the Speech Recognizer activity
    private void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        try {
            startActivityForResult(intent, SPEECH_REQUEST_CODE);
            etSearchView.setText("");
        } catch (ActivityNotFoundException a) {
            Snackbar.make(etSearchView, R.string.missing_recogn_app, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == MainActivity.RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);
            etSearchView.setText(spokenText); // observable will detect this
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initUI(View view) {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.view_actionbar_search);
        }

        // possible to use ButterKnife library for richer screens =)

        assert actionBar != null;
        actionBar.getCustomView().findViewById(R.id.ivSpeechRec)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        displaySpeechRecognizer();
                    }
                });

        etSearchView = (EditText) actionBar.getCustomView().findViewById(R.id.etSearchView);
        etSearchView.setOnEditorActionListener(mEditorActionListener);

        mAdapter = new ImageAdapter(getActivity());
        gvImages = (GridView) view.findViewById(R.id.gvImages);
        gvImages.setAdapter(mAdapter);

        tvEmpty = (TextView) view.findViewById(android.R.id.empty);

        mEditorActionListener = new android.widget.TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textview, int i, KeyEvent keyevent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    Utils.hideKeyboard(getActivity());
                    return true;
                } else {
                    return false;
                }
            }
        };
    }

    private void initRetrofit() {
        mSearchApi = SearcherRestAdapter.getRestAdapter().create(Api.SearchApi.class);
    }

    private void initScrollObservable() {
        Events.scrollEnd(gvImages).distinct(new Func1<MetaData, Integer>() {

            @Override
            public Integer call(MetaData triplet) {
                if (mAdapter.isEmpty()) {
                    return triplet.getTotalItemCount();
                } else {
                    return (mAdapter.getItem(0)).hashCode() + triplet.getTotalItemCount();
                }
            }

        }).filter(new Func1<MetaData, Boolean>() {
            // filter no connection state and empty text
            @Override
            public Boolean call(MetaData triplet) {
                return Utils.isNetworkConnected(getActivity()) && !TextUtils.isEmpty(etSearchView.getText());
            }
        }).flatMap(new Func1<MetaData, Observable<SearchResponse>>() {

            @Override
            public Observable<SearchResponse> call(MetaData metaData) {
                return mSearchApi.searchImage(Utils.getParametersMap(etSearchView.getText().toString(), metaData.getTotalItemCount()));
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<SearchResponse>() {

            @Override
            public void call(SearchResponse searchResponse) {
                if (searchResponse.responseData !=  null) {
                    mAdapter.update(searchResponse.responseData.results);
                }
            }
        });
    }

    private void initTextObservables() {
        Events.text(etSearchView).filter(new Func1<String, Boolean>() {

            @Override
            public Boolean call(String s) {
                boolean flag = true;
                if (s.length() <= 1) {
                    flag = false;
                }
                return flag;
            }

        }).throttleWithTimeout(500L, TimeUnit.MILLISECONDS).filter(new Func1<String, Boolean>() {
            // wait (avoid extra request) and filter request when no internet connection
            @Override
            public Boolean call(String s) {
                return Utils.isNetworkConnected(getActivity());
            }
        }).flatMap(new Func1<String, Observable<SearchResponse>>() {

            @Override
            public Observable<SearchResponse> call(String s) {
                return mSearchApi.searchImage(Utils.getParametersMap(etSearchView.getText().toString(), 0));
            }

        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<SearchResponse>() {

            public void call(SearchResponse searchResponse) {

                if (searchResponse.responseStatus == 200 && searchResponse.responseData != null
                        && searchResponse.responseData.results != null
                        && !searchResponse.responseData.results.isEmpty()) {

                    if (searchResponse.responseData.cursor.currentPageIndex == 0) {
                        mAdapter.clear();
                    }
                    mAdapter.update(searchResponse.responseData.results);
                    if (mAdapter.isEmpty()) {
                        tvEmpty.setText(getString(R.string.happy_face));
                    } else {
                        tvEmpty.setVisibility(View.GONE);
                    }

                } else if (searchResponse.responseStatus != 400) {
                    mAdapter.clear();
                    tvEmpty.setVisibility(View.VISIBLE);
                    tvEmpty.setText(getString(R.string.sad_face));
                }
            }
        });
    }
}
