package com.brucemax.imagesearcher.models;

/**
 * Use for pagination
 */
public class MetaData {
    private int firstVisibleItem;
    private int totalItemCount;
    private int visibleItemCount;

    public MetaData(int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.firstVisibleItem = firstVisibleItem;
        this.totalItemCount = totalItemCount;
        this.visibleItemCount = visibleItemCount;
    }

    public int getTotalItemCount() {
        return totalItemCount;
    }
}
