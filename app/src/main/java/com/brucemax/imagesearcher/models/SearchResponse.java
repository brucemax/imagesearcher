package com.brucemax.imagesearcher.models;

import java.util.List;

public class SearchResponse {
    public ResponseData responseData;
    public int responseStatus;

    public class ResponseData {
        public List<Result> results;
        public Cursor cursor;
    }

    public class Result {

        public String url; // image url
    }

    public class Cursor {
        public int currentPageIndex;
    }
}
