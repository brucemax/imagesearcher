package com.brucemax.imagesearcher.rx;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AbsListView;
import android.widget.TextView;
import com.brucemax.imagesearcher.models.MetaData;
import rx.Observable;
import rx.subjects.BehaviorSubject;

public class Events {

    public static Observable<MetaData> scrollEnd(AbsListView absListView) {
        final BehaviorSubject<MetaData> behaviorSubject = BehaviorSubject.create(new MetaData(0, 0, 0));
        absListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                boolean flag;
                flag = firstVisibleItem + visibleItemCount >= totalItemCount;
                if (flag && totalItemCount > 0) {
                    behaviorSubject.onNext(new MetaData(firstVisibleItem, visibleItemCount, totalItemCount));
                }
            }
        });
        return behaviorSubject;
    }

    public static Observable<String> text(TextView textview) {
        final BehaviorSubject<String> behaviorSubject = BehaviorSubject.create(textview.getText().toString());
        textview.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                behaviorSubject.onNext(editable.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        return behaviorSubject;
    }
}
