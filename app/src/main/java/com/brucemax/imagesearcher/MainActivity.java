package com.brucemax.imagesearcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // receiver and filter for detecting network changes
    private ConnectivityReceiver mConnectivityReceiver;
    private IntentFilter mConnectionFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mConnectivityReceiver = new ConnectivityReceiver();
        mConnectionFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(mConnectivityReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        registerReceiver(mConnectivityReceiver, mConnectionFilter);
        super.onResume();
    }

    private class ConnectivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!Utils.isNetworkConnected(MainActivity.this)) {
                Toast.makeText(MainActivity.this, "Check internet connection", Toast.LENGTH_LONG).show();
            }
        }
    }
}
