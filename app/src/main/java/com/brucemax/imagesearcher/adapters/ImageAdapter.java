package com.brucemax.imagesearcher.adapters;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.brucemax.imagesearcher.R;
import com.brucemax.imagesearcher.models.SearchResponse;
import com.brucemax.imagesearcher.widgets.SquaredImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends BaseAdapter {

    private List<SearchResponse.Result> mItems;
    private Context mContext;

    public ImageAdapter(Context mContext) {
        this.mContext = mContext;
        mItems = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems == null ? null : mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.image_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        SearchResponse.Result result = mItems.get(position);
        if (result.url != null && !result.url.isEmpty()) {
            Picasso.with(mContext).load(result.url)
                    .placeholder(new ColorDrawable(mContext.getResources().getColor(R.color.placeholder_color)))
                    .fit()
                    .centerCrop()
                    .into(viewHolder.image);
        }
        return convertView;
    }

    public void update(List<SearchResponse.Result> results) {
        mItems.addAll(results);
        notifyDataSetChanged();
    }

    public boolean isEmpty() {
        return mItems.isEmpty();
    }

    public void clear() {
        if(mItems == null) return;
        mItems.clear();
        notifyDataSetChanged();
    }


    public static class ViewHolder{
        private SquaredImageView image;

        public ViewHolder(View view) {
            image = (SquaredImageView) view.findViewById(R.id.squaredImageView);
        }
    }
}
