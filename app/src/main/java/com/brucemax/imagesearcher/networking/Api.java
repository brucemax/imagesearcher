package com.brucemax.imagesearcher.networking;

import com.brucemax.imagesearcher.models.SearchResponse;

import java.util.Map;

import retrofit.http.GET;
import retrofit.http.QueryMap;
import rx.Observable;

public class Api {
    public static final String DEV_HOST = "https://ajax.googleapis.com";
    public static final String PROD_HOST = "https://ajax.googleapis.com";
    public interface SearchApi {

        @GET("/ajax/services/search/images")
        Observable<SearchResponse> searchImage(@QueryMap Map<String, String> options);
    }
}
