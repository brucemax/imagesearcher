package com.brucemax.imagesearcher.networking;

import com.brucemax.imagesearcher.ImagerSearcherApplication;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class SearcherRestAdapter {

    public static RestAdapter getRestAdapter() {
        return new RestAdapter.Builder()
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Accept", "application/json");
                    }
                })
                .setLogLevel(ImagerSearcherApplication.IS_DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setEndpoint(ImagerSearcherApplication.IS_DEBUG ? Api.DEV_HOST : Api.PROD_HOST)
                .setErrorHandler(new SearcherErrorHandler())
                .build();
    }

    private static class SearcherErrorHandler implements ErrorHandler {

        @Override
        public Throwable handleError(RetrofitError cause) {

            return cause;

        }
    }
}
