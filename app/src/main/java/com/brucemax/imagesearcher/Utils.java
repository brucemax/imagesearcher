package com.brucemax.imagesearcher;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;

import static com.brucemax.imagesearcher.Utils.RequestParameters.*;

public class Utils {

    //params for search request
    public interface RequestParameters {
        String srart = "start";
        String query = "q";
        String safe = "safe";
        String itemCount = "rsz";
        String imgSize = "imgsz";
        String version = "v";
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static boolean isNetworkConnected(Context context) {
        NetworkInfo networkinfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return networkinfo != null && networkinfo.isConnectedOrConnecting();
    }

    public static HashMap<String, String> getParametersMap(String searchText, int start) {
        HashMap<String, String> map = new HashMap<>();
        map.put(srart, String.valueOf(start));
        map.put(query, searchText);
        map.put(safe, "moderate");
        map.put(itemCount, "8");
        map.put(imgSize, "medium");
        map.put(version, "1.0");
        return map;
    }
}
